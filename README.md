# Media entity consent

Enables users to give consent to load external media before their IP is sent to the external media providers. Inspired by tagesschau.de

Beware: This module will only work correctly when "Internal Page Cache" is disabled. Only "Internal Dynamic Page Cache" is allowed, to ensure the cookies are handled correctly.
